import BasePagePrueba from '../page_prueba_final/base.page.prueba';

const PAGE_TIMEOUT_PRUEBA = 10000

class BusquedaPagePrueba extends BasePagePrueba {

   //Elementos Web
   get resultado(){ return $('#center_column h5 > a') }

   //-------------------------------------------------------------------------------------------------------//
 
   /**
    * Click en el resultado de la búsqueda
    */
   ingresarAlResultado() {
       addStep('Ingresar al resultado');
       this.resultado.waitForClickable({timeout: PAGE_TIMEOUT_PRUEBA});
       this.resultado.click();
   }

   /**
    * Obtener texto del resultado de la búsqueda
    */
   obtenerNombreResultado() {
       addStep('Obtener nombre del resultado');
       return this.resultado.getText();
   }

}

export default new BusquedaPagePrueba();