import waitForDisplayed from 'webdriverio/build/commands/element/waitForDisplayed';
import BasePagePrueba from '../page_prueba_final/base.page.prueba';

const PAGE_TIMEOUT_PRUEBA = 30000

class ArticuloPagePrueba extends BasePagePrueba {
    // Elementos web
    get add_to_cart(){return $('#add_to_cart > button > span')};

    get titulo(){return $('h1')};

    get continueShopping(){return $('span[title = "Continue shopping"]')};

    get checkout(){ return $('a[title = "Proceed to checkout"] > span')}

    get cart(){ return $('.shopping_cart > a')}

    get precio(){ return $('#our_price_display')}

    get layerCart_form(){ return $('#layer_cart')}

    //Add to cart
    seleccionarAddtoCart(){
        addStep('Add to cart');
        //this.add_to_cart.waitForClickable({ timeout: PAGE_TIMEOUT_PRUEBA });
        this.add_to_cart.click();
    }

    //Obtener titulo del articulo
    obtenerTitulo() {
        addStep('Obtener Titulo');
        return this.titulo.getText()
    }

    //Seleccionar Continue shopping
    seleccionarContinueShopping(){
        addStep('Continue shopping');
        this.continueShopping.waitForClickable({timeout: PAGE_TIMEOUT_PRUEBA});
        this.continueShopping.click();
    }

    /**
     * Seleccionar Proceed to checkout
     */

    seleccionarCheckout(){
        addStep('Proceed to checkout');
        this.checkout.waitForClickable({timeout: PAGE_TIMEOUT_PRUEBA});
        this.checkout.click();
    }

    /**
     * Obtener precio del resultado
     * devuelve valor convertido a real (float)
     */

    obtenerPrecio(){
        addStep('Obtener Precio');
        let valor = this.precio.getText();
        valor = valor.substring(1,valor.length);
        valor = parseFloat(valor);
        return valor;
    }

    /**
     * Ir al carro de compras
     */

    ingresarCart(){
        addStep('Ir al carro de compras');
        this.cart.click();
    }

    validarLayerCart(){
        addStep('Validar Layer Cart');
        //expect(this.layerCart_form).to.be.visible() // Error: Invalid Chai property: visible
        this.layerCart_form.waitForDisplayed();
    }
}

export default new ArticuloPagePrueba();