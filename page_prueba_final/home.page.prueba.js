import BasePagePrueba from '../page_prueba_final/base.page.prueba';

const PAGE_TIMEOUT_PRUEBA = 10000

class HomePagePrueba extends BasePagePrueba {

   //WebElements
   get barraDeBusqueda(){ return $('[name="search_query"]') }

   get header(){ return $('.nav')}

   get login(){ return $('.login')}

   //-------------------------------------------------------------------------------------------------------//

   /**
    * Escribe el artículo en el campo de búsqueda y presiona Enter
    * @param {String} articulo que se buscará
    */
   buscar(articulo) {
      addStep( `Buscar ${articulo}`);
      super.clearAndSendKeys(this.barraDeBusqueda, articulo);
      this.barraDeBusqueda.keys('Enter');
   }

   /**
    * Obtener texto de la barra de búsqueda
    */
   obtenerTextoBusqueda() {
      addStep('Obtener texto de la barra de búsqueda');
       return this.barraDeBusqueda.getValue();
   }
   /**
    * Validacion visual 
    */
   headerVisualValidation() {
      addStep('Valdación visual Header')
      this.header.waitForDisplayed();
      expect(
         browser.checkElement(this.header, "app-header", {
         }),
         "Error: no coincide con la original"
      ).equal(0);
   }  
   /**
    *Selecciona el boton Sign in
    */
   abrirLogin(){
      addStep('Login')
      //this.login.waitForClickeable({timeout: PAGE_TIMEOUT_PRUEBA});
      this.login.click();
   }

}
export default new HomePagePrueba();