import BasePagePrueba from './base.page.prueba';

const PAGE_TIMEOUT_PRUEBA = 10000

class CheckoutPagePrueba extends BasePagePrueba {
    
    // Elementos del Carro

    get checkout(){ return $('=Proceed to checkout')}

    get create_account_form(){ return $('#create-account_form')}

    get login_form(){ return $('#login_form')}

    /**
     * Validar Create an Account form
     */

    validarCreateAccountForm(){
        addStep('Validar crear cuenta');
        //expect(this.create_account_form).to.be.visible;
        this.create_account_form.waitForDisplayed({timeout: PAGE_TIMEOUT_PRUEBA})       
    }
    //{Error: "no se muestra Create an account"}
    /**
     * Validar Create an Account form
     */

    validarLoginForm(){
        addStep('Validar sign in');
        //expect(this.login_form).to.be.visible({Error: "no se muestra Sign in"});
        this.login_form.waitForDisplayed({timeout: PAGE_TIMEOUT_PRUEBA})        
    }

    /**
    * Seleccionar Proceed to checkout
    */

    seleccionarCheckout(){
        addStep('Proceed to checkout');
        this.checkout.waitForClickable();
        this.checkout.click();
    }
}

export default new CheckoutPagePrueba();