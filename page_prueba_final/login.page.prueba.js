import BasePagePrueba from './base.page.prueba';

class LoginPagePrueba extends BasePagePrueba {

    //Elementos

    get email_create(){ return $('#email_create')}

    get email_input(){ return $('#email')}

    get password_input(){ return $('#passwd')}

    get submit_Login(){ return $('#SubmitLogin')}

    get submit_Create(){ return $('#SubmitCreate')}

    get create_account_form(){ return $('#create-account_form')}

    get login_form(){ return $('#login_form')}

    get error_CreateAccount(){ return $('#create_account_error')}

    get user(){ return $('.header_user_info span')}


    /**
     * Ingresa dirrección email
     */

    ingresarEmail(address){
        addStep('Ingresar email');
        super.clearAndSendKeys(this.email_input, address);
        //this.email_input.keys('Enter');
    }
    
    /**
     * Ingresar contraseña
     */

    ingresarPassword(password){
        addStep('Ingresar contraseña');
        super.clearAndSendKeys(this.password_input, password);
        //this.password_input.keys('Enter');
    }

    /**
     * Seleccionar Submit del Login
     */

    submitLogin(){
        addStep('Submit Login');
        //this.submit.waitForClickable();
        this.submit_Login.click();
    }

    /**
     * Validar modal de Login
     */

    modalLoginVisualValidation(){
        addStep('Validar modal de Login');
        this.login_form.waitForDisplayed();
        expect(
            browser.checkElement(this.login_form,"login_form",{
            }),
            "Error: no coincide con el original"
        ).equal(0);
    }

    /**
    * Validar modal Create an account
    */

    modalNewAccountVisualValidation(){
        addStep('Validar modal Create an account');
        this.create_account_form.waitForDisplayed();
        expect(
            browser.checkElement(this.create_account_form,"new_account_form",{
            }),
            "Error: no coincide con el original"
        ).equal(0);
    }

    /**
     * Obtener usuario
     */

    obtenerUser(){
        addStep('Obtener Usuario');
        this.user.waitForDisplayed();
        return this.user.getText();
    }

    /**
     * Seleccionar Create an Account Submit
     */

    submitCreateAccount(){
        addStep('Create an Account');
        this.submit_Create.waitForDisplayed();
        this.submit_Create.click();
    }

    /**
     * Validar mensaje de error al crear cuenta invalida
     */

     errorCreateAccountVisualValidation(){
        addStep('Validar mensaje de error al crear cuenta');
        this.error_CreateAccount.waitForDisplayed();
        expect(
            browser.checkElement(this.error_CreateAccount,"error_create_account",{
            }),
            "Error: no coincide con el original"
        ).equal(0);
    }

    /**
     * Validar mensaje de error al crear cuenta con email ya registrado
     */

     errorRegisteredEmail_CreateAccountVisualValidation(){
        addStep('Validar mensaje de error al crear cuenta con un email registrado ');
        this.error_CreateAccount.waitForDisplayed();
        expect(
            browser.checkElement(this.error_CreateAccount,"error_create_account_registeredEmail",{
            }),
            "Error: no coincide con el original"
        ).equal(0);
    }    

    /**
     * Ingresa email invalido a la entrada en Create an Account
     */

    ingresarInvalidEmail_CreateAccount(){
        addStep('Ingresar email invalido para crear cuenta');
        this.email_create.waitForDisplayed();
        super.clearAndSendKeys(this.email_create, 'invalidemail');
        this.email_create.keys('Enter');
    }

    /**
     * Ingresa email en uso a la entrada en Create an Account
     */

    ingresarRegisteredEmail_CreateAccount(){
        addStep('Ingresar email ya registrado para crear cuenta');
        this.email_create.waitForDisplayed();
        super.clearAndSendKeys(this.email_create, 'deyvismpf@gmail.com');
        this.email_create.keys('Enter');
    }
}

export default new LoginPagePrueba();