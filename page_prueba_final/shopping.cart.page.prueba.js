import BasePagePrueba from './base.page.prueba';

class CartPagePrueba extends BasePagePrueba {
    
    // Elementos del Carro

    get total_product(){ return $('.cart_total_price #total_product')}

    /**
     * Valida el precio total a pagar
     * recibe el valor de los productos añadidos al carrito y los compara
     */

    validarTotal(precio_total){
        addStep('Validar valor total');
        let total = this.total_product.getText();
        total = parseFloat(total.substring(1,total.length));
        //total = parseFloat(total);
        expect(precio_total).to.equal(total,{ Error: "precio total no coincide" });
    }

}

export default new CartPagePrueba();