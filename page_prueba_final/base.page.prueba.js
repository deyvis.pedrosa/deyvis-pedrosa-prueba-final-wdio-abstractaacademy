const PAGE_TIMEOUT_PRUEBA = 10000

export default class BasePagePrueba {


   /**
    * Abrir página
    * @param {String} ruta a la cual acceder
    */
   abrir(ruta) {
       addStep('abrir ${ruta}');
       browser.url(`${ruta}`);
   }


   /**
    * Esperar a que un elemento sea clickeable y hacer click
    * @param {Object} elemento a clickear
    */
   clickearElemento(elemento) {
       addStep('clickear ${elemento}');
       elemento.waitForClickable({ timeout: PAGE_TIMEOUT_PRUEBA });
       elemento.click();
   }


   /**
    * Método para enviar texto a un elemento
    * @param {Object} elemento que recibirá el texto
    * @param {String} texto a envíar 
    */
   clearAndSendKeys(elemento, texto){
       elemento.waitForClickable({ timeout: PAGE_TIMEOUT_PRUEBA });
       elemento.clearValue();
       elemento.click();
       elemento.keys(texto);
   }


}