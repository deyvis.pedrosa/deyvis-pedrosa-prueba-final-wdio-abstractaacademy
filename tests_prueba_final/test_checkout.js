import homePagePrueba from "../page_prueba_final/home.page.prueba";

import busquedaPagePrueba from "../page_prueba_final/busqueda.page.prueba";

import articuloPagePrueba from "../page_prueba_final/articulo.page.prueba";

import checkoutPagePrueba from "../page_prueba_final/checkout.page.prueba";

describe('MY STORE - Validation, sign in for checkout', function() {
    it('Checkout', function(){
        homePagePrueba.abrir('/');
        homePagePrueba.headerVisualValidation();
        let producto = 'Dress';
        homePagePrueba.buscar(producto);
        expect(homePagePrueba.obtenerTextoBusqueda()).to.equal(producto);
        expect(busquedaPagePrueba.obtenerNombreResultado()).to.contain(producto);
        busquedaPagePrueba.ingresarAlResultado();
        expect(articuloPagePrueba.obtenerTitulo()).to.contain(producto);
        articuloPagePrueba.seleccionarAddtoCart();
        articuloPagePrueba.validarLayerCart();
        articuloPagePrueba.seleccionarCheckout();
        checkoutPagePrueba.seleccionarCheckout();
        checkoutPagePrueba.validarCreateAccountForm();
        checkoutPagePrueba.validarLoginForm();
        browser.debug();      
    });
});