import homePagePrueba from '../page_prueba_final/home.page.prueba';

import loginPagePrueba from '../page_prueba_final/login.page.prueba';

describe('MY STORE - Login', function () {
        it(`Validar Login`, function () {
            homePagePrueba.abrir('/');
            homePagePrueba.headerVisualValidation();
            homePagePrueba.abrirLogin();
            loginPagePrueba.modalLoginVisualValidation();
            let address = 'deyvismpf@gmail.com';
            let password = 'u3YwYG!68ipgBQE';
            let usuario = 'Deyvis Pedrosa';
            loginPagePrueba.ingresarEmail(address);
            loginPagePrueba.ingresarPassword(password);
            loginPagePrueba.submitLogin();
            expect('.page-heading').to.have.text('MY ACCOUNT','Error: no se mostró el header esperado');
            expect(loginPagePrueba.obtenerUser()).to.equal(usuario);
            browser.debug();
        });  
});