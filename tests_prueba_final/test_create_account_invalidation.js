import loginPagePrueba from "../page_prueba_final/login.page.prueba";

import homePagePrueba from "../page_prueba_final/home.page.prueba";

describe('MY STORE - Create Account Invalidation', function(){
    it('Create account invalid inputs', function(){
        homePagePrueba.abrir('/');
        homePagePrueba.headerVisualValidation();
        homePagePrueba.abrirLogin();
        loginPagePrueba.modalLoginVisualValidation();
        loginPagePrueba.modalNewAccountVisualValidation();
        loginPagePrueba.submitCreateAccount();
        loginPagePrueba.errorCreateAccountVisualValidation();
        loginPagePrueba.ingresarInvalidEmail_CreateAccount();
        loginPagePrueba.errorCreateAccountVisualValidation();
        loginPagePrueba.ingresarRegisteredEmail_CreateAccount();
        loginPagePrueba.errorRegisteredEmail_CreateAccountVisualValidation();
        browser.debug();
    });
});