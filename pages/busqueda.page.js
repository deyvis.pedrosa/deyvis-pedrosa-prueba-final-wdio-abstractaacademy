import BasePage from '../pages/base.page';

class BusquedaPage extends BasePage {

   //Elementos Web
   get resultado(){ return $('h4') }

   //-------------------------------------------------------------------------------------------------------//
 
   /**
    * Click en el resultado de la búsqueda
    */
   ingresarAlResultado() {
       addStep('ingresar al resultado');
       super.clickOnElement(this.resultado);
   }

   /**
    * Obtener texto del resultado de la búsqueda
    */
   obtenerNombreResultado() {
       addStep('obtener nombre del resultado');
       return this.resultado.getText();
   }

}

export default new BusquedaPage();