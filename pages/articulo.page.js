import BasePage from '../pages/base.page';

class ArticuloPage extends BasePage {
    // Elementos web
    get lista_colores(){return $('select')}

    //seleccionar index
    seleccionarColor(index){
        addStep('seleccionar color');
        this.lista_colores.selectByIndex(index);
    }
}

export default new ArticuloPage();