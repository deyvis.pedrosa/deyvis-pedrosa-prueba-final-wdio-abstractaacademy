import BasePage from '../pages/base.page';

class HomePage extends BasePage {

   //WebElements
   get barraDeBusqueda(){ return $('[name="search"]') }

   //-------------------------------------------------------------------------------------------------------//

   /**
    * Escribe el artículo en el campo de búsqueda y presiona Enter
    * @param {String} articulo que se buscará
    */
   buscar(articulo) {
    addStep('Buscar artículo: ${articulo}');
       super.clearAndSendKeys(this.barraDeBusqueda, articulo);
       this.barraDeBusqueda.keys('Enter');
   }

   /**
    * Obtener texto de la barra de búsqueda
    */
   obtenerTextoBusqueda() {
    addStep('Obtener texto de la barra de búsqueda');
       return this.barraDeBusqueda.getValue();
   }

}
export default new HomePage();