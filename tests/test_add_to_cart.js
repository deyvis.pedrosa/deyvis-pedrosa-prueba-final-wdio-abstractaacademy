import homePagePrueba from '../page_prueba_final/home.page.prueba';

import busquedaPagePrueba from '../page_prueba_final/busqueda.page.prueba';

import articuloPagePrueba from '../page_prueba_final/articulo.page.prueba';

import DATOS from '../datos/productos';

describe('MY STORE - Cart', function () {
    DATOS.forEach(({producto}) => {
        it(`Buscar ${producto}`, function () {
            homePagePrueba.abrir('/');
            homePagePrueba.headerVisualValidation();
            homePagePrueba.buscar(producto);
            expect(homePagePrueba.obtenerTextoBusqueda()).to.equal(producto);
            expect(busquedaPagePrueba.obtenerNombreResultado()).to.contain(producto);
            busquedaPagePrueba.ingresarAlResultado();
            expect(articuloPagePrueba.obtenerTitulo()).to.contain(producto);
            articuloPagePrueba.seleccionarAddtoCart();
            articuloPagePrueba.validarLayerCart(); 
            articuloPagePrueba.seleccionarContinueShopping();  
            browser.debug();
        });
    });  
});