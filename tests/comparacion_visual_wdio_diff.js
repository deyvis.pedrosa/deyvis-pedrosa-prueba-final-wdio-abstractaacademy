describe('Opencart', function () {
    it("Comparación de imágenes con página de WebdriverIO", () => {
        browser.url("https://webdriver.io");
        $(".navbar--fixed-top").waitForDisplayed();
        expect(
            browser.checkElement($(".navbar--fixed-top"), "wdio-headerContainer", {
                /* opciones de configuración para el elemento */
            }),
            "Error: la barra de navegación de WebdriverIO no coincide con la original"
        ).equal(0);
        browser.url('https://webdriver.io/blog/');
        $('.navbar--fixed-top').waitForDisplayed();
        expect(browser.checkElement($('.navbar--fixed-top'), 'wdio-headerContainer', {})).to.not.equal(0);
    });
});