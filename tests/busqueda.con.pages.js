
import homePage from '../pages/home.page';

import busquedaPage from '../pages/busqueda.page';

import DATOS from '../datos/articulos';

 describe('Búsqueda', function () {
    DATOS.forEach(({articulo}) => {
        it(`Debería buscar ${articulo}`, function () {
            homePage.abrir('/');
            homePage.buscar(articulo);
            expect(homePage.obtenerTextoBusqueda()).to.equal(articulo);
            expect(busquedaPage.obtenerNombreResultado()).to.equal(articulo);
        });
    });
 });