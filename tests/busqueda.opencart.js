describe('Carrito', function () {
    it('Debería buscar apple cinema, ingresar al artículo y seleccionar un color', function () {
        browser.url('/');
        let barraDeBusqueda = $('[name="search"]');
        barraDeBusqueda.setValue('apple cinema');
        assert.equal(barraDeBusqueda.getValue(),'apple cinema','Error: no se mostró el texto esperado en la barra de búsqueda');
        barraDeBusqueda.keys('Enter');
        browser.debug();
        assert.equal($('h4').getText(),'Apple Cinema 30"','Error: no se mostró el header esperado');
        let product = $('*=Apple Cinema');
        product.click();
        assert.equal($('h1').getText(),'Apple Cinema 30"','Error: no se mostró el header esperado');
        let dropDownColor = $('select');
        dropDownColor.selectByIndex(2);
        console.log(dropDownColor.getValue());
        browser.debug();
    });
 });