import homePage from '../pages/home.page';

import articuloPage from '../pages/articulo.page';


describe('Carrito', function () {
    it('Debería buscar iphone', function () {
        homePage.abrir('/');
        let articulo = 'apple cinema';
        homePage.buscar(articulo);
        expect(homePage.obtenerTextoBusqueda()).to.equal(articulo);
        let product = $('*=Apple Cinema');
        product.click();
        browser.debug();
        let index = 2;
        articuloPage.seleccionarColor(index);
        browser.debug();
    });
});