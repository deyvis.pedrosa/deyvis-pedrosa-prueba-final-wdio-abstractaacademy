describe('Carrito', function () {
    it('Debería buscar iphone', function () {
        browser.url('/');
        let barraDeBusqueda = $('[name="search"]');
        barraDeBusqueda.setValue('iphone');
        assert.equal(barraDeBusqueda.getValue(),'iphone','Error: no se mostró el texto esperado en la barra de búsqueda');
        barraDeBusqueda.keys('Enter');            
        assert.equal($('h4').getText(),'iPhone','Error: no se mostró el header esperado');
 });
});