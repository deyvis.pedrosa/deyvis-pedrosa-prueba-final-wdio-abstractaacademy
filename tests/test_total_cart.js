import articuloPagePrueba from '../page_prueba_final/articulo.page.prueba';

import homePagePrueba from '../page_prueba_final/home.page.prueba';

import busquedaPagePrueba from '../page_prueba_final/busqueda.page.prueba';

import shoppingCartPagePrueba from '../page_prueba_final/shopping.cart.page.prueba';

describe('MY STORE - Total Cart', function() {
    it('Total', function(){
        homePagePrueba.abrir('/');
        homePagePrueba.headerVisualValidation();
        let producto = 'Dress';
        homePagePrueba.buscar(producto);
        expect(homePagePrueba.obtenerTextoBusqueda()).to.equal(producto);
        expect(busquedaPagePrueba.obtenerNombreResultado()).to.contain(producto);
        busquedaPagePrueba.ingresarAlResultado();
        expect(articuloPagePrueba.obtenerTitulo()).to.contain(producto);
        let precio_1 = articuloPagePrueba.obtenerPrecio();
        articuloPagePrueba.seleccionarAddtoCart();
        articuloPagePrueba.seleccionarContinueShopping();
        producto = 'Shirt';
        homePagePrueba.buscar(producto);
        busquedaPagePrueba.ingresarAlResultado();
        let precio_2 = articuloPagePrueba.obtenerPrecio();
        articuloPagePrueba.seleccionarAddtoCart();
        articuloPagePrueba.seleccionarContinueShopping();
        articuloPagePrueba.ingresarCart();
        let precio_total =  precio_1 + precio_2;
        shoppingCartPagePrueba.validarTotal(precio_total)
        //browser.debug();
    });
});